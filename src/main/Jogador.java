package main;

import java.util.ArrayList;

public class Jogador {
	
	private int somaCartas = 0;
	private String nomeJogador = "";
	
	ArrayList<Carta> macoJogador = new ArrayList<>();
	
	public void somarCartas(int valorCarta) {
		somaCartas = somaCartas + valorCarta;
		
	}

	public int getSomaCartas() {
		return somaCartas;
	}
	
	public void setNomeJogador(String nome) {
		nomeJogador = nome;
	}
	
	public String getNomeJogador() {
		return nomeJogador;
	}
}
