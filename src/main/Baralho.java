package main;

import java.util.ArrayList;
import java.util.Collections;

public class Baralho {
	
	ArrayList<Carta> maco = new ArrayList<>();
		
	public Baralho() {
		montarMaco();
		embaralharMaco();
	}
	
	private void embaralharMaco() {
		Collections.shuffle(maco);
	}
	
	private void montarMaco() {
		
		for(int i=0; i<Carta.retornarNaipes().length ; i++) {
			for(int j=0 ; j<Carta.retornarValores().length; j++) {
				Carta carta = new Carta(j,i);
				maco.add(carta);
			}
		}		
	}
		
}
