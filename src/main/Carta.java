package main;

import java.util.HashMap;

public class Carta {

	private static String[] naipes = {"Copas", "Ouros", "Espadas", "Paus"};
	private static String[] valorLiteral = {"As", "Dois", "Tres", "Quatro", "Cinco", "Seis", "Sete", "Oito", "Nove", "Dez", "Valete", "Dama", "Reis"};
		
	private int naipe;
	private int valor;
	
	public Carta(int v, int n) {
		naipe = n;
		valor = v;
	}
	
	public String retornarNaipe() {
		return naipes[naipe];
	}
	
	public String retornarValor() {
		return valorLiteral[valor];
	}
	
	public static String[] retornarNaipes() {
		return naipes;
	}
	
	public static String[] retornarValores() {
		return valorLiteral;
	}
	
	public String imprimirCarta() {
		return valorLiteral[valor]+" de "+naipes[naipe];				
	}
	
}
