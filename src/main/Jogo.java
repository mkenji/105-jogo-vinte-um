package main;

import java.util.HashMap;
import java.util.Scanner;

public class Jogo {

	static Baralho baralho1 = new Baralho();
	static Jogador jogador1 = new Jogador();
	static Jogador jogador2 = new Jogador();
	static int numCartasIniciais = 2;
	static HashMap<String,Integer> valorReal = new HashMap<String,Integer>();

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String menu = "S";
		int count = 2;
		jogador1.setNomeJogador("JOGADOR 1");
		jogador2.setNomeJogador("JOGADOR 2");
		
		montarValoresReaisJogo();
		for(int i=0; i<numCartasIniciais; i++) {
			jogar(jogador1);
			jogar(jogador2);
		
		}

		
		if(jogador1.getSomaCartas() > 21) {
			System.out.println("Voce Perdeu!");
		} 
		if(jogador1.getSomaCartas() ==21) {
			System.out.println("Voce Ganhou!");
		}
		if(jogador1.getSomaCartas() < 21) {
			System.out.println("Voce Desistiu!");
		}
		
	}

	public static void jogar(Jogador jogador) {
		int ordemCarta = baralho1.maco.size() -1;
		Carta cartaSorteada = baralho1.maco.get(ordemCarta);
		atribuirCartaJogador(cartaSorteada, jogador, ordemCarta);
		jogador.somarCartas(valorReal.get(cartaSorteada.retornarValor()));
		
		int ordemCartaJogador = jogador.macoJogador.size()-1;
		System.out.println(jogador.getNomeJogador());
		System.out.println(jogador.macoJogador.get(ordemCartaJogador).imprimirCarta());
		System.out.println("Valor real da carta"+ valorReal.get(cartaSorteada.retornarValor()));
		System.out.println("Soma das cartas: "+jogador.getSomaCartas());
		
	}
	
	public static void executarJogadas(Jogador jogador) {
		
	}
	
	public static void atribuirCartaJogador(Carta cartaSorteada, Jogador jogador, int ordemCarta) {
		jogador.macoJogador.add(cartaSorteada);
		baralho1.maco.remove(ordemCarta);
	}

	private static void montarValoresReaisJogo() {
		valorReal.put("As", 1);
		valorReal.put("Dois", 2);
		valorReal.put("Tres", 3);
		valorReal.put("Quatro", 4);
		valorReal.put("Cinco", 5);
		valorReal.put("Seis", 6);
		valorReal.put("Sete", 7);
		valorReal.put("Oito", 8);
		valorReal.put("Nove", 9);
		valorReal.put("Dez", 10);
		valorReal.put("Valete", 10);
		valorReal.put("Dama", 10);
		valorReal.put("Reis", 10);
	}
	

}
